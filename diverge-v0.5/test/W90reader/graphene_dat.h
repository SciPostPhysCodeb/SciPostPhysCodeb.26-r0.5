#ifndef DIVERGE_SKIP_TESTS

#pragma once

#ifdef __cplusplus
extern "C" {
#endif

const char* gen_graphene_hr_dat( void );

#ifdef __cplusplus
}
#endif

#endif // DIVERGE_SKIP_TESTS
