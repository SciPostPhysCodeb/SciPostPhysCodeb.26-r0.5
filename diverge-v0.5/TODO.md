# TODOs
- doc: structured arrays in python (done?), struct datatype usage example
  symmetries from python (emery)
- code: include static self-energies in both grid and npatch, with fourier
  selection and cutoff in npatch
- checkpointing of flow step
- c library functions for reading model files
- BS23 integrator reimplement. Have a nice little piece of C code to do all the
  BS stuff. What's left is to include this in the backends...
