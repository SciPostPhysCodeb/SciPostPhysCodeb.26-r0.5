#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import diverge
diverge.init(None, None)
diverge.run_tests()
diverge.finalize()
