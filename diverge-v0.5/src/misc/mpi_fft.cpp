#include "mpi_fft.hpp"
#include "mpi_functions.h"

fftw_mpi_plan::fftw_mpi_plan( int dims_[3], index_t howmany, complex128_t* in,
        complex128_t* out, int dir, complex128_t* buf ) {
    index_t dims[3] = {dims_[0], dims_[1], dims_[2]};
    constructor( dims, howmany, in, out, dir, buf );
}

fftw_mpi_plan::fftw_mpi_plan( index_t dims[3], index_t howmany, complex128_t* in,
        complex128_t* out, int dir, complex128_t* buf ) {
    constructor( dims, howmany, in, out, dir, buf );
}

#if defined(USE_MPI_FFTW) && defined(USE_MPI)
#include <fftw3-mpi.h>

// TODO checking allocations for some simulations before removing this...
static inline complex128_t* fftw_alloc_complex_wrap_( size_t sz, const char* f, int l ) {
    complex128_t* result = (complex128_t*)fftw_alloc_complex( sz );
    mpi_vrb_printf( "allocating %.2fGiB (%s:%i)\n",
            (double)(sz*sizeof(complex128_t)) / (double)(1024.*1024.*1024.), f, l );
    return result;
}
#define fftw_alloc_complex_wrap( sz ) fftw_alloc_complex_wrap_( sz, __FILE__, __LINE__ )


void fftw_mpi_plan::constructor( index_t dims_[3], index_t howmany_, complex128_t* in_,
        complex128_t* out_, int dir, complex128_t* buf_ ) {
    in = in_;
    out = out_;
    howmany = howmany_;
    nranks = diverge_mpi_comm_size();
    rank = diverge_mpi_comm_rank();
    dims[0] = dims_[0];
    dims[1] = dims_[1];
    dims[2] = dims_[2];
    tvec[0] = tvec[1] = tvec[2] = 0.0;
    timings_enabled = true;
    ptrdiff_t alloc = fftw_mpi_local_size_many( 3, dims, howmany, FFTW_MPI_DEFAULT_BLOCK,
            diverge_mpi_get_comm(), &local_n0, &local_0_start );
    in_mpi = (complex128_t*)fftw_alloc_complex_wrap(alloc);
    if (in != out)
        out_mpi = (complex128_t*)fftw_alloc_complex_wrap(alloc);
    else
        out_mpi = in_mpi;

    if (buf_) {
        buf = buf_;
        free_buf = false;
    } else {
        buf = (complex128_t*)fftw_alloc_complex_wrap(howmany*dims[0]*dims[1]*dims[2]);
    }

    p = fftw_mpi_plan_many_dft( 3, dims, howmany, FFTW_MPI_DEFAULT_BLOCK, FFTW_MPI_DEFAULT_BLOCK,
            (fftw_complex*)in_mpi, (fftw_complex*)out_mpi, diverge_mpi_get_comm(), dir, FFTW_MEASURE );

    counts = (int*)calloc(nranks, sizeof(int));
    displs = (int*)calloc(nranks, sizeof(int));
    int count = local_n0*dims[1]*dims[2]*howmany;
    int displ = local_0_start*dims[1]*dims[2]*howmany;
    diverge_mpi_allgather_int( &count, counts, 1 );
    diverge_mpi_allgather_int( &displ, displs, 1 );
}

void fftw_mpi_plan::execute( complex128_t* in, complex128_t* out, bool input_transposed ) {
    double tick = 0, tock = 0;

    if (!input_transposed) {

        if (timings_enabled) tick = diverge_mpi_wtime();
        // reorder from (h,i+l0,j) to (i,j,h)
        #pragma omp parallel for collapse(3)
        for (index_t h=0; h<howmany; ++h)
        for (index_t i=0; i<local_n0; ++i)
        for (index_t j=0; j<dims[1]*dims[2]; ++j)
            in_mpi[IDX3( i, j, h, dims[1]*dims[2], howmany )] =
                in[IDX3( h, i+local_0_start, j, dims[0], dims[1]*dims[2] )];
        if (timings_enabled) tock = diverge_mpi_wtime();
        tvec[0] += tock-tick;

    } else {

        if (timings_enabled) tick = diverge_mpi_wtime();
        memcpy( in_mpi, in + local_0_start * dims[1] * dims[2] * howmany,
                sizeof(complex128_t) * local_n0 * dims[1] * dims[2] * howmany );
        if (timings_enabled) tock = diverge_mpi_wtime();
        tvec[0] += tock-tick;

    }

    if (timings_enabled) tick = diverge_mpi_wtime();
    fftw_mpi_execute_dft( p, (fftw_complex*)in_mpi, (fftw_complex*)out_mpi );
    if (timings_enabled) tock = diverge_mpi_wtime();
    tvec[1] += tock-tick;

    if (!input_transposed) {

        if (timings_enabled) tick = diverge_mpi_wtime();
        memcpy( buf+displs[rank], out_mpi, counts[rank]*sizeof(complex128_t) );
        if (timings_enabled) tock = diverge_mpi_wtime();
        tvec[0] += tock-tick;

        if (timings_enabled) tick = diverge_mpi_wtime();
        diverge_mpi_allgatherv( buf, counts, displs );
        if (timings_enabled) tock = diverge_mpi_wtime();
        tvec[2] += tock-tick;

        if (timings_enabled) tick = diverge_mpi_wtime();
        // reorder from (i,j,h) to (h,i+l0,j)
        #pragma omp parallel for collapse(2)
        for (index_t h=0; h<howmany; ++h)
        for (index_t r=0; r<nranks; ++r) {
            complex128_t* start = buf+displs[r];
            index_t _local_0_start = displs[r]/howmany/dims[1]/dims[2],
                    _local_n0 = counts[r]/howmany/dims[1]/dims[2];

            for (index_t i=0; i<_local_n0; ++i)
            for (index_t j=0; j<dims[1]*dims[2]; ++j) {
                out[IDX3( h, _local_0_start+i, j, dims[0], dims[1]*dims[2] )] =
                    start[IDX3( i, j, h, dims[1]*dims[2], howmany )];
            }
        }
        if (timings_enabled) tock = diverge_mpi_wtime();
        tvec[0] += tock-tick;

    } else {

        if (timings_enabled) tick = diverge_mpi_wtime();
        memcpy( out+displs[rank], out_mpi, counts[rank]*sizeof(complex128_t) );
        if (timings_enabled) tock = diverge_mpi_wtime();
        tvec[0] += tock-tick;

        if (timings_enabled) tick = diverge_mpi_wtime();
        diverge_mpi_allgatherv( out, counts, displs );
        if (timings_enabled) tock = diverge_mpi_wtime();
        tvec[2] += tock-tick;

    }
}

fftw_mpi_plan::~fftw_mpi_plan() {
    if (in_mpi != out_mpi)
        fftw_free(out_mpi);
    fftw_free(in_mpi);
    if (free_buf) fftw_free(buf);
    fftw_destroy_plan(p);
    free(counts);
    free(displs);
}
#else // USE_MPI_FFTW && USE_MPI
void fftw_mpi_plan::constructor( index_t dims_[3], index_t howmany_, complex128_t* in,
        complex128_t* out, int dir, complex128_t* buf_ ) {
    (void)buf_;
    tvec[0] = tvec[1] = tvec[2] = 0.0;
    timings_enabled = true;
    dims[0] = dims_[0];
    dims[1] = dims_[1];
    dims[2] = dims_[2];
    howmany = howmany_;
    local_0_start = 0;
    local_n0 = dims[0];
    int mydims[3] = { static_cast<int>(dims_[0]), static_cast<int>(dims_[1]), static_cast<int>(dims_[2]) };
    p = fftw_plan_many_dft( 3, mydims, howmany, (fftw_complex*)in, mydims, 1, mydims[0]*mydims[1]*mydims[2],
            (fftw_complex*)out, mydims, 1, mydims[0]*mydims[1]*mydims[2], dir, FFTW_MEASURE );
}

void fftw_mpi_plan::execute( complex128_t* in, complex128_t* out, bool input_transposed ) {
    double tick = 0, tock = 0;
    if (timings_enabled) tick = diverge_mpi_wtime();
    if (!input_transposed) {
        fftw_execute_dft( p, (fftw_complex*)in, (fftw_complex*)out );
    } else {
        complex128_t* buf = (complex128_t*)fftw_alloc_complex( dims[0]*dims[1]*dims[2]*howmany );

        // transpose
        for (index_t i=0; i<dims[0]*dims[1]*dims[2]; ++i)
        for (index_t h=0; h<howmany; ++h)
            buf[IDX2( h, i, dims[0]*dims[1]*dims[2] )] = in[IDX2( i, h, howmany )];

        // fft
        if (in == out) {
            fftw_execute_dft( p, (fftw_complex*)buf, (fftw_complex*)buf );
        } else {
            fftw_execute_dft( p, (fftw_complex*)buf, (fftw_complex*)out );
            memcpy( buf, out, sizeof(complex128_t)*dims[0]*dims[1]*dims[2]*howmany );
        }

        // transpose
        for (index_t i=0; i<dims[0]*dims[1]*dims[2]; ++i)
        for (index_t h=0; h<howmany; ++h)
            out[IDX2( i, h, howmany )] = buf[IDX2( h, i, dims[0]*dims[1]*dims[2] )];

        fftw_free( (fftw_complex*)buf );
    }
    if (timings_enabled) tock = diverge_mpi_wtime();
    tvec[1] += tock-tick;
}

fftw_mpi_plan::~fftw_mpi_plan() {
    fftw_destroy_plan(p);
}
#endif // USE_MPI_FFTW && USE_MPI
