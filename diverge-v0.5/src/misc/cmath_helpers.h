/* Copyright (C) 2023 Jonas B. Hauck, Lennart Klebl; GPLv3+ (LICENSE.md) */

#pragma once

#ifdef __cplusplus
extern "C" {
#endif

double cmath_sin( double x );
double cmath_cos( double x );
double cmath_pow( double x, double y );

#ifdef __cplusplus
}
#endif
