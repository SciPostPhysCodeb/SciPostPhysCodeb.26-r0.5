/* Copyright (C) 2023 Jonas B. Hauck, Lennart Klebl; GPLv3+ (LICENSE.md) */

#pragma once
#include "../diverge_model.h"
#include "../diverge_internals_struct.h"


#ifdef __cplusplus
extern "C" {
#endif

void merge_rs_orb(diverge_model_t* model);


#ifdef __cplusplus
}
#endif
