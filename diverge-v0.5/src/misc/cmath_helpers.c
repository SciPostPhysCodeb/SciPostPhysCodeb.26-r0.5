/* Copyright (C) 2023 Jonas B. Hauck, Lennart Klebl; GPLv3+ (LICENSE.md) */

#include "cmath_helpers.h"
#include <math.h>

double cmath_sin( double x ) { return sin(x); }
double cmath_cos( double x ) { return cos(x); }
double cmath_pow( double x, double y ) { return pow(x, y); }
