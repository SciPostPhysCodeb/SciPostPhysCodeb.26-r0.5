/* Copyright (C) 2023 Jonas B. Hauck, Lennart Klebl; GPLv3+ (LICENSE.md) */

#pragma once

#ifdef __cplusplus
extern "C" {
#endif

// Function: diverge_compilation_status
void diverge_compilation_status( void );

#ifdef __cplusplus
}
#endif
