/* Copyright (C) 2023 Jonas B. Hauck, Lennart Klebl; GPLv3+ (LICENSE.md) */

#include "diverge_post.h"

diverge_postprocess_conf_t diverge_postprocess_conf_defaults_CPP( void ) {
    return diverge_postprocess_conf_defaults;
}
