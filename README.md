# Codebase release 0.5 for divERGe

by Jonas B. Profe, Dante M. Kennes, Lennart Klebl

SciPost Phys. Codebases 26-r0.5 (2024) - published 2024-02-08

[DOI:10.21468/SciPostPhysCodeb.26-r0.5](https://doi.org/10.21468/SciPostPhysCodeb.26-r0.5)

This repository archives a fixed release as a citable refereed publication. Please refer to the [publication page](https://scipost.org/SciPostPhysCodeb.26-r0.5) to view the full bundle and get information on how to cite it.

For a link to the latest/development version, check the Resources section below.

Copyright Jonas B. Profe, Dante M. Kennes, Lennart Klebl.

This README is published under the terms of the CC BY 4.0 license. For the license to the actual codebase, please refer to the license specification in the codebase folder.

## Resources:

* Codebase release version (archive) repository at [https://git.scipost.org/SciPostPhysCodeb/SciPostPhysCodeb.26-r0.5](https://git.scipost.org/SciPostPhysCodeb/SciPostPhysCodeb.26-r0.5)
* Live (external) repository at [https://git.rwth-aachen.de/frg/diverge/-/tree/v0.5?ref_type=tags](https://git.rwth-aachen.de/frg/diverge/-/tree/v0.5?ref_type=tags)
